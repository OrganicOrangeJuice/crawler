from bs4 import BeautifulSoup
from lxml.html import fromstring
from fake_useragent import UserAgent
import requests
import time


class Proxy:
    """
    Returns a proxy address from free-proxy-list.net
    Usage: p = Proxy().__next__() => Gets A proxy Generator
           next(p) => will return the next proxy address
    """
    def __init__(self):
        self.counter = 1
        self.current_pos = 0
        self.current_proxy = None

    @property
    def generator(self):
        return self.__next__()

    def __next__(self):
        while True:
            for i in fromstring(
                    requests.get('https://free-proxy-list.net/').text).xpath('//tbody/tr')[self.current_pos:self.counter]:
                if i.xpath('.//td[7][contains(text(),"yes")]'):
                    yield ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            self.current_pos = self.counter
            self.counter += 1


class ProxyFactory:
    proxy_gen = Proxy().generator
    proxy = next(proxy_gen)

    @classmethod
    def new_proxy(cls):
        cls.proxy = next(cls.proxy_gen)

    @classmethod
    def get_proxy(cls):
        return cls.proxy


class Page:
    def __init__(self, url, page_number):
        self.url = url
        self.page_number = page_number
        self.headers = {'User-Agent': UserAgent().random}
        self.request_status = None

    def __str__(self):
        return f'Url for Page => {self.url}, Page => {self.page_number}'

    def request_page(self, img=None):
        url = self.url
        if img:
            url = img

        while True:
            try:
                proxy = ProxyFactory.get_proxy()
                print(f'Requesting page: {url}, with proxy: {proxy}')
                self.request_status = requests.get(url, headers=self.headers, proxies={
                    "http": proxy, "https": proxy}, stream=True, timeout=2)

            except Exception:
                ProxyFactory.new_proxy()
                print('[Failed]')

            yield self.request_status


class Chapter:
    def __init__(self):
        self._pages = []

    def __len__(self):
        return len(self._pages)

    def __iter__(self):
        return self._pages.__iter__()

    def __str__(self):
        return str([page.__str__() for page in self._pages])

    def __getitem__(self, item):
        return self._pages[item]

    def __repr__(self):
        return f'{self.__class__.__qualname__} contains => {len(self._pages)} pages'

    def append(self, page):
        if isinstance(page, Page):
            self._pages.append(page)
        else:
            raise TypeError('Expected a Page Type')

    def extend(self, lst):
        for element in lst:
            if isinstance(element, Page):
                self._pages.append(element)
            else:
                raise TypeError('Expected a Page Type')


class ReadmsMangaChapter(Chapter):
    # Do more a general approach?
    def __init__(self, url):
        super().__init__()
        self._url = 'https://readms.net/r/' + url

        # get all the links to the pages in the dropdown list
        pages = self.find_pages()

        # add all the missing pages not listed
        # in the dropdown list
        for counter, page in enumerate(pages, start=1):
            link_to_chapter = f'https://readms.net{page}'
            base_link = link_to_chapter.rfind('/') + 1

            page_number = link_to_chapter[base_link:]

            if page_number != str(counter):  # add the rest
                missing_chapters = [Page(link_to_chapter[:base_link] + str(n), n)
                                    for n in range(counter, int(page_number) + 1)]
                self.extend(missing_chapters)
            else:
                self.append(Page(link_to_chapter, counter))
        print('[Done]')

    def find_pages(self, sleep_timer=0.5):

        # get the first initial page, so we can find all the other pages
        # via web crawling
        page = Page(self._url, page_number=0)

        # get the content of the first page
        while next(page.request_page()) is None:
            time.sleep(sleep_timer)

        if page.request_status.status_code is not requests.codes.ok:
            raise ConnectionError('Request denied!', page.request_status)
        else:
            print('[Succeeded]')

        soup_instance = BeautifulSoup(page.request_status.content, 'html.parser')

        print('Crawling for pages..')
        # get all the pages on the site

        pages = [link.find("a").get("href")
                 for link in soup_instance.find_all("div", attrs={"class": "btn-group btn-reader-page"})
                 for link in link.find_all('li')]

        return pages


