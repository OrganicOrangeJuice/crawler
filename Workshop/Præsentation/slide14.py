"""
Solution 3
"""


# brug a reductions
def longest_word1(filename):
    with open(filename, 'r') as infile:
        words = infile.read().split()
    max_len = len(max(words, key=len))
    return [word for word in words if len(word) == max_len]


def longest_word2(filename):
    with open(filename, 'r') as file:
        words = file.read().split()

    biggest_so_far = ''
    for word in words:
        if len(word) > len(biggest_so_far):
            biggest_so_far = word
    return biggest_so_far

print(longest_word1('opgave3.txt'))
print(longest_word2('opgave3.txt'))
