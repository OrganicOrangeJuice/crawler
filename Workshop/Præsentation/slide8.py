"""
Tuple, namedTuple, List, Set, Dictionary & defaultDict
"""

# Disse 5 data typer, bliver brugt 95 % af alle python programmer
# har man styr på dem, så er man godt på vej til at overtage verden

"""
Tuple
"""

# * Kendt for at pakke/udpakke værdier f.eks. en række i en database
# * man laver en tuple ved hjælp af --> ()
# * Anvendelse
#  Record = (val1, val2, val3)
#  a, b, c = record
#  val = record[n]

row = ('Henrik', 'R. Gullaksen', 'Vejle')
print(row[0], row[1], row[2])

# Hvad er problemet?










# Hvordan løser vi det?
# Namedtuple ftw
# Man skal dog først importerer collections

from collections import namedtuple  # as ntuple
person = namedtuple('person', ['firstname', 'lastname', 'address'])
row = person('Henrik', 'R. Gullaksen', 'Vejle')
print(row.firstname, row.lastname, row.address)

# problem solved!


"""
List
"""

# * Kendt for håndhæve rækkefølge, Array
# * man laver en liste ved hjælp af --> []
# * Anvendelse

# * items [val1, val2, val3, val4, ..., valn]
# * items[0:n]
# * x = items[n]
# * del items[n]
# * items.append(n)
# * items.sort()
# ...


def some_function():
    pass


l = ['str', 42, some_function(), ['lemon']]



"""
Set
"""

# * Kendt for Unikhed, medlemstest
# * man laver et set ved hjælp af --> {}
# * Anvendelse

# s = {val1, val2, ..., valn}
# s.add(val)
# s.remove(val)
# val in s
# ...

# Fra set teorien f.eks. A∩B (ven diagram)
# Forskellen mellem en liste og et set er følgende
# Jonas forekommer 2 gange, men listen gemmer alt hvad du giver den og håndhæver order
names1 = ['Roman', 'Matias', 'Julie', 'Sebastian', 'Charlie', 'Jonas', 'Jonas']
print(names1)

# I et set, ville alle duplikationer blive fjernet, du få kun alle unikke elementer
# husk at rækkefølgen ikke altid er den samme
names2 = {'Roman', 'Matias', 'Julie', 'Sebastian', 'Charlie', 'Jonas', 'Jonas'}
names3 = {'Christina', 'Morten', 'Julie', 'Elisa', 'Per', 'Paul', 'Jonas'}

# lav en  A∩B
print([x for x in names2 if x in names3])


"""
Dictionary
"""
# * Kendt for lookup tables/Mappning, indexsering
# * man laver en dictionary ved hjælp af --> {}

# * Anvendelse

# d = {key1: val1, ..., keyn: valn}
# val = d[key]
# d[key] = val
# del d[key]
# key in d
# ...

prices = {'OrangeJuice': 42, 'LemonJuice': 24}
print(prices['OrangeJuice'])

for key, val in prices:
    print(key, val)

# Handy Type  at kende

"""
DefaultDict
"""

# * Kendt for MultiDict, en til mange relationer, gruppering
# husk at importerer den fra collections module
# anvendelse:
# d = defaultdict(list)
# d[key].append(val)
# values = d[key]
#
# d = defaultdict(set)
# d[key].add(val)
# unique_values = d[key]

from collections import defaultdict
d = defaultdict(list)
d['juice'].append('orange')
d['juice'].append('lemon')
d['juice'].append('apple')

d['cars'].append('ford')
d['cars'].append('bmw')
d['cars'].append('fiat')

print(d)
print(d['juice'], d['cars'])


