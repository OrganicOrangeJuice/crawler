"""
Input /Læse og Skrive
"""

# man kan få input fra en bruger ved hjælp af input() funktionen

color = input("what's your favourite color?")

# Eller læse/skrive fra en fil

# with er noger some hedder en context manager
# som sørger for cleanup

with open("opgave3.txt", "rw") as f:
    text = f.read()

# or read line by line

with open("opgave3.txt", "rw+") as f:
    for line in f:
        print(line)

"""
'r' --> Open file for reading ( default )
'w' --> Open a file for writing, creates a new file if it doest exist, or truncates the file if it exist
'a' --> Open for appending
'b' --> Open in binary mode
'+' --> Open a file for updating ( reading and writing )
... ( read the doc ) 
"""

