
"""
Variables & String manipulation
"""

# En intenger
intVar = 32

# En float
floatVar = 3.14

# En String
stringVar = 'I Like Oranges'

# Streng manipulation
print(stringVar.split())
print(stringVar[0:3])  # slicing
print(stringVar[-3:])
print(stringVar[0:-3])
print(stringVar.replace('Oranges', 'Lemons'))



