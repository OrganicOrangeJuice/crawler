"""
Comprehensions
"""

# Comprehensions ligner rigtig meget set-builder-notation
# bruges til at genererer lister, sets, dictionaries
# kan redusere din kode

"""
List Comprehension
[expression for x in iterable if condition]
"""

# lav en list a de første 10 kvadrat tal
l = [x*x for x in range(10)]
print(l)

# er det samme som:
l = []
for x in range(10):
    l.append(x*x)
print(l)


"""
Set Comprehension
{expresision for x in iterable if condition}
"""

# {x | x ∈ of intengers, -2 < x < 14 }

s = {x for x in range(-2, 14) if -2 < x < 14}

"""
Dict Comprehension
{key: val for key, val in iterable if condition}
"""

# lav en list a de første 10 kvadrat tal, med en index

d = {key: val*val for key, val in enumerate(range(10))}

# Søg selv på generator expressions.
# De er rigtige gode hvis man skal arbejde med enorm meget data



