"""
Iterationer
"""

container = []

# Tænkt på det som en foreach

for item in container:
    print(item)

# der findes ikke en for loop some vi kender fra C# eller c++/c, java ...
# for(var i = 0; i<10; i++) {}
# hjælpe metode: range() to the rescue!
# range(start, slut, retning)


# default range(0, n, 1 )
for i in range(0, 10):  # 0, 1, 2, 3, 4 ... 9
    print(i)

# hvad med baglæns?
for i in range(10, 0, -1):  # 10, 9 ,8 ,7 .. 1
    print(i)

# While loop er som vi kender den.
n = 0
while n < 10:
    print(n)
    n = n + 1

# Nogle handy build in funktioner

# enumerate returnerer index og en item i en container

for index, item in enumerate(container):
    print(index, item)

# zip tager to 'iterables' og fletter dem sammen

for item in zip(container, container):
    print(item)

for item1, item2 in zip(container, container):
    print(item1, item2)

