"""
Klasser
"""


# Klasser indenholder noget som hedder Dunder/Magic - metoder
# bruges i forbindelse med operator overloading
# slå selv op hvad de forskellige dunder metoder hedder/gør

class Spam:
    def __init__(self, a, b, c): # Constructor
        self.a = a  # self --> this
        self.b = b
        self.c = c

    def foo(self):
        pass


s1 = Spam(a='a',b='b',c='c') # Named arguments virker også her
s2 = Spam('a','b','c') # samt positional
s1.foo()

# Læs selv mere om python klasser da der er forskel på hvordan de virker end det
# man er vandt til fra c# og andre programmerings sprog