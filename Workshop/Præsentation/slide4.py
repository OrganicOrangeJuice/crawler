"""
Conditional Statements
"""

# or, and, not, is, xor ...

val1 = True
val2 = False

if val1 or val2:  # if expression: logic
    if val1 and val2:
        pass  # 'pass' betyder skip. Kan sammenlignes med: if(val) {} <-- tomme brackets
    else:
        pass
elif val2 and not val1:
    pass
else:
    pass

