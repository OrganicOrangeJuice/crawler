"""
Solution 2
"""

even_numbers1 = {x for x in range(0, 100) if (x % 2) == 0}
even_numbers2 = {x*2 for x in range(0,100) if x*2 < 100}

print(even_numbers1)
print(even_numbers2)


