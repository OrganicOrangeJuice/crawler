"""
HOW TO PYTHON
 af Henrik Roman Gullaksen
"""

# * Python er et "Interpreted" programmings-sprog (Læs & Execute)
# * Skal ikke kompileres
# * Bruger Blanktegn/Mellemrum istedet for brackets '{}'
# * Python interpreter

# Eks.
def some_code():
    if True:
        pass
    else:
        pass
    print(':)')

