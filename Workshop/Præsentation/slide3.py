"""
Functions
"""


def some_function(a, b, c):
    print(a, b, c)


some_function('val1', 42, 'val2')  # Positional Arguments
some_function(a='val1', b='val2', c='42')  # Keyword/Named Arguments

